from flask import Flask, render_template, request
import sqlite3
from flask_mysqldb import MySQL

# this code runs with mysql
# first run XAMPP, then flask run

app = Flask(__name__)

app.config['MYSQL_HOST'] = '127.0.0.1'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'flask'

mysql = MySQL(app)

# def get_db_connection():
#     conn = sqlite3.connect('database.db')
#     conn.row_factory = sqlite3.Row
#     return conn

@app.route('/', methods = ['POST', 'GET'])
def index():
    #conn = get_db_connection()
    #posts = conn.execute('SELECT * FROM posts').fetchall()
    #conn.close()
    if request.method == 'GET':
        cursor = mysql.connection.cursor()
        # filter db
        query = 'SELECT * FROM doctors'
        parameters = []
        print('ARGS: ', request.args.items())
        for key, value in request.args.items():
            if value:
                if key == 'specialty':
                    value = '\'' + value + '\''
                if key == 'price_limit':
                    query += f" AND price<={value}"
                else:
                    query += f" AND {key}={value}"

        query = query.replace("AND", "WHERE", 1)
        print('QUERY:', query)
        cursor.execute(query)
        doctors = cursor.fetchall()
        cursor.close()
    else:
        cursor = mysql.connection.cursor()
        cursor.execute('''SELECT * FROM doctors''')
        doctors = cursor.fetchall()
        cursor.close()

    return render_template('index.html', doctors=doctors)

if __name__ == "__main__":
    app.run(host='localhost', port=5000, debug=True)

@app.route('/utku/')
def utku():
    return '<h3>This is a page for dr utku.</h3>'
